# -*- coding: utf-8 -*-
"""
Created on Wed Aug  8 14:51:19 2018

@author: gehammo
"""

import numpy as np

template = '''REGION %s
  COORDINATES
    %.1f %.1f %.1f
    %.1f %.1f %.1f
  /
END

'''

template2 = '''REGION %s
  COORDINATES
    %.5e %.5e %.5e
    %.5e %.5e %.5e
  /
END

'''

def WriteRegion(name,bounds):
  if np.amax(np.abs(np.array(bounds))) > 1.e9:
    return template2%(name,bounds[0][0],bounds[0][1],bounds[0][2],
                           bounds[1][0],bounds[1][1],bounds[1][2])
  else:
    return template%(name,bounds[0][0],bounds[0][1],bounds[0][2],
                          bounds[1][0],bounds[1][1],bounds[1][2])

inf = 1.e20
x_start = 0.
x_salt = 8.
x_wp = 112.
x_borehole = -999.
x_pcs1 = 224.
x_sror = 256.
x_pcs2 = 536.
x_nror = 568.
x_pcs3 = 848.
x_ops = 912.
xs_shaft = 1216.
xe_shaft = 1224.
x_exp = 1216.
x_salt2 = 1936.
x_buffer = 2040.
x_end = 2048.

y_start = 0.
y_salt = 8.
ys_wp = 80.
ye_wp = 176.
ys_pcs1 = 120
ye_pcs1 = 136.
ys_sror = 48.
ye_sror = 208.
ys_pcs2 = 104.
ye_pcs2 = 152.
ys_nror = 32.
ye_nror = 224.
ys_pcs3 = 104.
ye_pcs3 = 152.
ys_ops = 112.
ye_ops = 144
ys_shaft = 120.
ye_shaft = 136.
ys_exp = 96.
ye_exp = 160.
y_buffer = 248.
y_end = 256.

z_start = 0.
z_salt = 8.
z_drz1 = 12.
zs_mb139 = z_drz1
ze_mb139 = zs_mb139 + 4.
z_wp = 20.
z_drz2 = 28.
zs_mbab = 32.
ze_mbab = zs_mbab + 4.
z_mb138 = 40.
z_salt2 = 44.
z_buffer = 52.
z_end = 56.

buffer_up = [[x_start,-inf,-inf],[x_salt,inf,inf]]
buffer_dn = [[x_buffer,-inf,-inf],[x_end,inf,inf]]
salt = [[x_salt,-inf,-inf],[x_buffer,inf,inf]]

mb138 = [[x_salt,-inf,z_mb138],[x_buffer,inf,z_salt2]]
mbab = [[x_salt,-inf,zs_mbab],[x_buffer,inf,ze_mbab]]
mb139 = [[x_salt,-inf,zs_mb139],[x_buffer,inf,ze_mb139]]

wp = [[x_wp,ys_wp,z_wp],
      [x_pcs1,ye_wp,z_drz2]]
pcs1 = [[x_pcs1,ys_pcs1,z_wp],
        [x_sror,ye_pcs1,z_drz2]]
sror = [[x_sror,ys_sror,z_wp],
        [x_pcs2,ye_sror,z_drz2]]
pcs2 = [[x_pcs2,ys_pcs2,z_wp],
        [x_nror,ye_pcs2,z_drz2]]
nror = [[x_nror,ys_nror,z_wp],
        [x_pcs3,ye_nror,z_drz2]]
pcs3 = [[x_pcs3,ys_pcs3,z_wp],
        [x_ops,ye_pcs3,z_drz2]]
ops = [[x_ops,ys_ops,z_wp],
       [x_exp,ye_ops,z_drz2]]
exp = [[x_exp,ys_exp,z_wp],
       [x_salt2,ye_exp,z_drz2]]
shaft_bot = [[xs_shaft,ys_shaft,ze_mb139],
             [xe_shaft,ye_shaft,zs_mbab]]
shaft_mid = [[xs_shaft,ys_shaft,ze_mbab],
             [xe_shaft,ye_shaft,z_mb138]]
shaft_top = [[xs_shaft,ys_shaft,z_salt2],
             [xe_shaft,ye_shaft,z_end]]

zs = z_drz1
ze = z_mb138
drz_wp = [[x_wp,ys_wp,zs],
          [x_pcs1,ye_wp,ze]]
drz_pcs1 = [[x_pcs1,ys_pcs1,zs],
            [x_sror,ye_pcs1,ze]]
drz_sror = [[x_sror,ys_sror,zs],
            [x_pcs2,ye_sror,ze]]
drz_pcs2 = [[x_pcs2,ys_pcs2,zs],
            [x_nror,ye_pcs2,ze]]
drz_nror = [[x_nror,ys_nror,zs],
            [x_pcs3,ye_nror,ze]]
drz_pcs3 = [[x_pcs3,ys_pcs3,zs],
            [x_ops,ye_pcs3,ze]]
drz_ops = [[x_ops,ys_ops,zs],
           [x_exp,ye_ops,ze]]
drz_exp = [[x_exp,ys_exp,zs],
           [x_salt2,ye_exp,ze]]

zs = z_mb138
ze = z_salt2
mb138_wp = [[x_wp,ys_wp,zs],
          [x_pcs1,ye_wp,ze]]
mb138_pcs1 = [[x_pcs1,ys_pcs1,zs],
            [x_sror,ye_pcs1,ze]]
mb138_sror = [[x_sror,ys_sror,zs],
            [x_pcs2,ye_sror,ze]]
mb138_pcs2 = [[x_pcs2,ys_pcs2,zs],
            [x_nror,ye_pcs2,ze]]
mb138_nror = [[x_nror,ys_nror,zs],
            [x_pcs3,ye_nror,ze]]
mb138_pcs3 = [[x_pcs3,ys_pcs3,zs],
            [x_ops,ye_pcs3,ze]]
mb138_ops = [[x_ops,ys_ops,zs],
           [x_exp,ye_ops,ze]]
mb138_exp = [[x_exp,ys_exp,zs],
           [x_salt2,ye_exp,ze]]

mb139_pcs1 = [[x_pcs1,ys_pcs1,zs_mb139],
               [x_sror,ye_pcs1,ze_mb139]]
mb139_pcs2 = [[x_pcs2,ys_pcs2,zs_mb139],
               [x_nror,ye_pcs2,ze_mb139] ]         
mb139_pcs3 = [[x_pcs3,ys_pcs3,zs_mb139],
               [x_ops,ye_pcs3,ze_mb139]]
mb139_shaft = [[xs_shaft,ys_shaft,zs_mb139],
               [xe_shaft,ye_shaft,ze_mb139]]

mbab_pcs1 = [[x_pcs1,ys_pcs1,zs_mbab],
              [x_sror,ye_pcs1,ze_mbab]]
mbab_pcs2 = [[x_pcs2,ys_pcs2,zs_mbab],
              [x_nror,ye_pcs2,ze_mbab] ]         
mbab_pcs3 = [[x_pcs3,ys_pcs3,zs_mbab],
              [x_ops,ye_pcs3,ze_mbab]]
mbab_shaft = [[xs_shaft,ys_shaft,zs_mbab],
              [xe_shaft,ye_shaft,ze_mbab]]

f = open('test.txt','w')
f.write(WriteRegion('buffer_up',buffer_up))
f.write(WriteRegion('buffer_dn',buffer_dn))
f.write(WriteRegion('salt',salt))
f.write(WriteRegion('mb139',mb139))
f.write(WriteRegion('mbab',mbab))
f.write(WriteRegion('mb138',mb138))
f.write(WriteRegion('drz_wp',drz_wp))
f.write(WriteRegion('drz_pcs1',drz_pcs1))
f.write(WriteRegion('drz_sror',drz_sror))
f.write(WriteRegion('drz_pcs2',drz_pcs2))
f.write(WriteRegion('drz_nror',drz_nror))
f.write(WriteRegion('drz_pcs3',drz_pcs3))
f.write(WriteRegion('drz_ops',drz_ops))
f.write(WriteRegion('drz_exp',drz_exp))
f.write(WriteRegion('mb138_wp',mb138_wp))
f.write(WriteRegion('mb138_pcs1',mb138_pcs1))
f.write(WriteRegion('mb138_sror',mb138_sror))
f.write(WriteRegion('mb138_pcs2',mb138_pcs2))
f.write(WriteRegion('mb138_nror',mb138_nror))
f.write(WriteRegion('mb138_pcs3',mb138_pcs3))
f.write(WriteRegion('mb138_ops',mb138_ops))
f.write(WriteRegion('mb138_exp',mb138_exp))
f.write(WriteRegion('wp',wp))
f.write(WriteRegion('pcs1',pcs1))
f.write(WriteRegion('sror',sror))
f.write(WriteRegion('pcs2',pcs2))
f.write(WriteRegion('nror',nror))
f.write(WriteRegion('pcs3',pcs3))
f.write(WriteRegion('ops',ops))
f.write(WriteRegion('exp',exp))
f.write(WriteRegion('mb139_pcs1',mb139_pcs1))
f.write(WriteRegion('mb139_pcs2',mb139_pcs2))
f.write(WriteRegion('mb139_pcs3',mb139_pcs3))
f.write(WriteRegion('mb139_shaft',mb139_shaft))
f.write(WriteRegion('mbab_pcs1',mbab_pcs1))
f.write(WriteRegion('mbab_pcs2',mbab_pcs2))
f.write(WriteRegion('mbab_pcs3',mbab_pcs3))
f.write(WriteRegion('mbab_shaft',mbab_shaft))
f.write(WriteRegion('shaft_bot',shaft_bot))
f.write(WriteRegion('shaft_mid',shaft_mid))
f.write(WriteRegion('shaft_top',shaft_top))
f.close()

